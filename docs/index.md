# Taipy 시작하기

이 초보자 친화적인 가이드로 Taipy로 뛰어들어 보세요. 설치, 구성 및 첫 번째 응용 프로그램을 쉽게 만드는 법을 배울 수 있다.

![](./images/result_01.webp)

## pip으로 설치

1. **필수 구성 요소**: Python(3.8과 3.11 사이의 버전)이 설치되어 있는지 확인하고 [pip](https://pip.pypa.io/)을 설치한다.
2. **설치 명령**: 터미널 또는 명령 프롬프트에서 다음을 실행한다.

```bash
$ pip install taipy
```

다른 설치 방법이나 Python 또는 pip으로 부족한 경우 [설치 페이지](https://docs.taipy.io/en/release-3.0/installation/)를 참조하세요.

## 첫 Taipy 시나리오
Taipy 시나리오는 파이프라인 실행을 모델링한다. 작업이나 함수가 협업하여 데이터를 교환하는 실행 그래프라고 생각할 수 있다. 여러분은 여러분의 시나리오가 얼마나 복잡하든 완전히 통제할 수 있다.

기본적인 "Hello World" 시나리오를 만들어 보겠다.

![](./images/hello_world.svg)

그래프는 다음과 같다.

- `input_name`이라는 입력 데이터 노드
- 함수 `build_message()`를 참조하는 작업으로 `input_name` 데이터 노드를 처리하여 `message` 데이터 노드를 출력한다.
- **구성**: 다음의 Python 코드로 실행 그래프를 설정한다.

``` python
from taipy import Config


def build_message(name: str):
    return f"Hello {name}!"


input_name_data_node_cfg = Config.configure_data_node(id="input_name")
message_data_node_cfg = Config.configure_data_node(id="message")
build_msg_task_cfg = Config.configure_task("build_msg", build_message, input_name_data_node_cfg, message_data_node_cfg)
scenario_cfg = Config.configure_scenario("scenario", task_configs=[build_msg_task_cfg])
```

> **코드에 line number를 넣는 방법을 찾아 볼 것**

- 위 코드의 설명은 다음과 같다.
    - 4-5 행은 태스크가 실행 중에 사용할 함수를 정의한다.
    - 8-9 행는 데이터 노드, `input_name`과 `message`를 구성한다.
    - 10 행은 `build_message()` 함수와 연관된 `build_msg`라는 작업을 구성하여 입력과 출력 데이터 노드를 지정한다.
    - 마지막으로 11 행은 이전에 구성된 작업를 제공하는 시나리오의 실행 그래프를 구성한다.

- **Core 서비스 초기화**: Core 서비스는 이전 단계의 구성을 처리하여 시나리오 관리 기능을 설정한다.

```python
from taipy import Core

if __name__ == "__main__":
    Core().run()
```

- **시나리오와 데이터 관리**: Core 서비스를 실행하면 시나리오를 생성과 관리하고 실행을 위한 작업 그래프를 제출하며 데이터 노드를 액세스할 수 있다.

```python
import taipy as tp

hello_scenario = tp.create_scenario(scenario_cfg)
hello_scenario.input_name.write("Taipy")
hello_scenario.submit()
print(hello_scenario.message.read())
```

- 위 코드의 설명은 다음과 같다.
    - 3 행에서 `tp.create_scenario()` 메서드는 이전에 구축된 시나리오 구성에서 새로운 시나리오 이름 `hello_scenario`를 인스턴스화한다.
    - 4 행은 `write()` 메서드를 이용하여 `hello_scenario`의 입력 데이터 노드 `input_name`을 문자열 값을 "`Taipy`"로 설정한다.
    - 5 행은 실행을 위한 `hello_scenario`를 제출하고, 이는 잡(job)의 생성과 실행을 트리거한다. 이 잡은 입력 데이터 노드를 읽고, 그 값을 `build_message()` 함수에 전달하고, 그 결과를 출력 데이터 노드에 기록한다.
    - 6 행은 `hello_scenario` 시나리오의 실행으로 작성된 출력 데이터 노드 `message`를 읽고 출력한다.

- **어플리케이션 실행**: Taipy CLI 명령으로

```bash
$ taipy run hello_world_scenario.py
```

- 아래와 같은 결과가 예상된다.

```
[2023-02-08 20:19:35,062][Taipy][INFO] job JOB_build_msg_9e5a5c28-6c3e-4b59-831d-fcc8b43f882e is completed.
Hello Taipy!
```

## 그래픽 인터페이스 구축
Taipy의 Python API를 사용하여 시나리오를 처리했지만, 일반적으로 Taipy를 사용하여 만든 그래픽 인터페이스와 원활하게 작동하여 보다 사용자 친화적인 경험을 제공한다. 다음은 "Hello World" 시나리오를 위해 설정된 간단한 GUI이다.

```python
import taipy as tp

# Previous configuration of scenario
...

page = """
Name: <|{input_name}|input|>
<|submit|button|on_action=submit_scenario|>

Message: <|{message}|text|>
"""

input_name = "Taipy"
message = None


def submit_scenario(state):
    state.scenario.input_name.write(state.input_name)
    state.scenario.submit(wait=True)
    state.message = state.message.read()


if __name__ == "__main__":
    tp.Core().run()
    scenario = tp.create_scenario(scenario_cfg)
    tp.Gui(page).run()
```

이제 위 코드의 주요 요소를 설명하겠다.

```python
import taipy as tp


page = """
Name: <|{input_name}|input|>
<|submit|button|on_action=submit_scenario|>
Message: <|{message}|text|>
"""
```

### 페이지
Taipy 페이지는 마크다운, Html 또는 Python과 같은 여러 방식으로 정의될 수 있다. 페이지 변수는 사용자 인터페이스의 마크다운 표현이다. 이는 시각적 요소뿐만 아니라 표준 마크다운 구문을 사용한다.

### 시각적 요소(Visual Element)
Taipy는 Python 변수 및 환경과 상호 작용할 수 있는 다양한 시각적 요소를 제공한다. 변수를 표시하고 수정하며 어플리케이션과 상호 작용할 수 있다.

`<|{variable}|visual_element_type|...|>`는 일반적인 예이다. `variable`은 시각적 요소의 주요 속성이며, 일반적으로 시각적 요소를 통해 표시되거나 수정되는 것이다.

초기 예에서는 다음과 같다.

- `input_name`은 입력과 텍스트 필드에 바인딩되어 사용자의 입력을 `input_name` 변수에 직접 저장할 수 있다.
- `message`는 텍스트 필드에 연결되어 변경 내용을 사용자에게 디스플레이할 수 있다.

### 행동을 통한 상호작용
`on_action=submit_scenario`같은 행동(action)은 버튼과 같은 시각적 요소가 특정 기능을 트리거하도록 하여 어플리케이션의 상호 작용성을 향상시킨다.

```python
def submit_scenario(state):
    scenario.input_name.write(state.input_name)
    scenario.submit()
    state.message = scenario.message.read()
```

`submit_scenario()`를 포함한 모든 콜백은 [`tate`](https://docs.taipy.io/en/release-3.0/manuals/reference/taipy.gui.State) 객체를 첫 번째 파라미터로 수신한다. 이 state는 사용자의 연결을 나타내며 사용자가 어플리케이션과 상호 작용하는 동안 변수를 읽고 설정하는 데 사용된다. Taipy가 여러 사용자를 동시에 처리할 수 있도록 해준다.

`senario`같은 변수는 전역 변수이므로 `senario`는 모든 사람이 사용할 수 있는 반면, `state.input_name`과 `state.input`은 그들과 상호작용하는 사용자에게 특정된다.  이러한 설계는 각 사용자의 행동이 개별적이고 효율적으로 제어되도록 보장한다.

`submit_scenario()` 함수에서, 사용자가 인터페이스에서 입력한 `input_name`은 시나리오에 저장된다. 제출 후, 결과는 검색되고 `message` 변수에 저장되며, 이는 그 후 사용자 인터페이스 상에 표시된다.

```python
if __name__ == "__main__":
    tp.Core().run()
    scenario = tp.create_scenario(scenario_cfg)
    tp.Gui(page).run()
```

어플리케이션의 주요 부분은 Core 서비스를 설정하고 시나리오를 생성한 후 GUI를 시작하는 것으로 시작되며, 이는 대화형 인터페이스를 활성화하고 기능하게 만든다.

![](./images/result_02.webp)

